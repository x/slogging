====================
Welcome to slogging!
====================

slogging is a stats and logging tools for OpenStack Swift.

* License: Apache license
* Source: https://git.openstack.org/cgit/openstack/slogging
* Bugs: https://bugs.launchpad.net/slogging
