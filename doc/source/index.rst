..
      Copyright 2010-2012 OpenStack Foundation
      All Rights Reserved.

      Licensed under the Apache License, Version 2.0 (the "License"); you may
      not use this file except in compliance with the License. You may obtain
      a copy of the License at

          http://www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
      WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
      License for the specific language governing permissions and limitations
      under the License.

====================================
Welcome to slogging's documentation!
====================================

slogging is a stats and logging tools for OpenStack Swift which is
composed of three parts parts: log creation, log uploading, and log processing.

The system handles two types of logs (access and account stats),
but it can be extended to handle other types of logs.


.. toctree::
   :maxdepth: 1

   user/index
   install/index
   contributor/index
