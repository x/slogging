=================
How to Contribute
=================

Contributor License Agreement
-----------------------------

.. index::
   single: license; agreement

In order to contribute to the slogging project, you need to have
signed OpenStack's contributor's agreement.

.. seealso::

   * http://docs.openstack.org/infra/manual/developers.html
   * http://wiki.openstack.org/CLA

LaunchPad Project
-----------------

Most of the tools used for OpenStack depend on a launchpad.net ID for
authentication.

.. seealso::

   * https://launchpad.net
   * https://launchpad.net/slogging

Project Hosting Details
-------------------------

Bug tracker
    http://launchpad.net/slogging

Code Hosting
    https://git.openstack.org/cgit/openstack/slogging

Code Review
    https://review.openstack.org/#/q/status:open+project:openstack/slogging,n,z

