================================
How to Install with RPM Packages
================================

#. Install RPM Package::

    sudo rpm -ivh slogging-[slogging-version].noarch.rpm

#. Check if the RPM Package has successfully installed::

    sudo rpm -qa | grep slogging

