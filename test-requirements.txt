# The order of packages is significant, because pip processes them in the order
# of appearance. Changing the order has an impact on the overall integration
# process, which may cause wedges in the gate later.

# Hacking already pins down pep8, pyflakes and flake8
hacking<0.12,>=0.11.0 # Apache-2.0
coverage!=4.4 # Apache-2.0
nose # LGPL
nosexcover # BSD
nosehtmloutput # Apache-2.0
flake8<=2.5.5

# Security checks
bandit>=1.1.0 # Apache-2.0
